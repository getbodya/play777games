<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="../../css/test.css">
</head>
<body>
    <nav class="navbar clearfix">
        <div class="container">
            <ul class="nav">
                <li><a href="/admin">Dashboard</a></li>
                <li><a href="/admin/simulations">Simulations</a></li>
                <li><a href="/admin/statistics">Statistic</a></li>
                <li><a href="/admin/percent-manager">Percent Manager</a></li>
            </ul>
        </div>
    </nav>

    <br>
    <br>
    <br>
    <br>
    <br>

    @yield('content')
</body>
</html>
